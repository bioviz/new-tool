import java.util.ArrayList;

import org.opencv.core.Scalar;

public class ColorspaceData {

   public class Colorspace {
      String spaceName;
      Scalar colorspace;
      
      public Colorspace(String spaceName, Scalar colorspace) {
         this.spaceName = spaceName;
         this.colorspace = colorspace;
      }
      
      public void printName() {
         System.out.println(spaceName);
      }
      
      public void printColorspace() {
         System.out.println(colorspace.toString());
      }      
   }
   
   String imgName;
   ArrayList<Colorspace> colorspaces;
   
   public ColorspaceData(String name) {
      colorspaces = new ArrayList<Colorspace>();
      imgName = name;
   }
   
   public void addScalar(String name, double[] vals) {
      colorspaces.add(new Colorspace(name, new Scalar(vals)));
   }
   
   public void printContents() {
      System.out.println("Image Name: " + imgName);
      for(Colorspace s : colorspaces) {
         s.printName();
         s.printColorspace();
      }
      System.out.println();
   }
   
}
