import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.image.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.fxml.*;

import org.opencv.core.Core;

public class UI extends Application {

    public static final int INPUT_IMG_WIDTH = 300;
    public static final int THRESH_IMG_WIDTH = 300;
    public static final int OUTPUT_IMG_WIDTH = 500;
    //UI shit
    @FXML private BorderPane root;
    //inputs:
    @FXML private ChoiceBox<String> algoList;
    @FXML private ChoiceBox<String> inputList;
    @FXML private Slider hueSlider_min;    @FXML private Slider hueSlider_max;
    @FXML private Slider satSlider_min;    @FXML private Slider satSlider_max;
    @FXML private Slider valSlider_min;    @FXML private Slider valSlider_max;
    @FXML private ImageView input;
    //outputs:
    @FXML private ImageView threshold;
    @FXML private ImageView output;
    @FXML private Label count;

    //State variable container
    private AlgoState state;

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            this.state = new AlgoState();
            
            this.root = FXMLLoader.<BorderPane>load(getClass().getResource("UI.fxml"));
            Scene scene = new Scene(this.root, 400, 400);

            this.algoList = (ChoiceBox<String>) this.root.lookup("#algoList");
            this.algoList.getItems().addAll(this.state.getAlgoSet());
            this.algoList.getSelectionModel().selectFirst();

            this.inputList = (ChoiceBox<String>) this.root.lookup("#inputList");
            this.inputList.getItems().addAll(this.state.getInputSet());
            this.inputList.getSelectionModel().selectFirst();

            this.hueSlider_min = (Slider) this.root.lookup("#hueSlider_min");
            this.hueSlider_max = (Slider) this.root.lookup("#hueSlider_max");
            this.satSlider_min = (Slider) this.root.lookup("#satSlider_min");
            this.satSlider_max = (Slider) this.root.lookup("#satSlider_max");
            this.valSlider_min = (Slider) this.root.lookup("#valSlider_min");
            this.valSlider_max = (Slider) this.root.lookup("#valSlider_max");

            this.input = (ImageView) this.root.lookup("#input");
            this.input.setPreserveRatio(true);
            this.input.setFitWidth(INPUT_IMG_WIDTH);

            this.threshold = (ImageView) this.root.lookup("#threshold");
            this.threshold.setPreserveRatio(true);
            this.threshold.setFitWidth(THRESH_IMG_WIDTH);
            
            this.output = (ImageView) this.root.lookup("#output");
            this.output.setPreserveRatio(true);
            this.output.setFitWidth(OUTPUT_IMG_WIDTH);

            this.count = (Label) this.root.lookup("#count");

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Execute the current algorithm on the current input.
     **/
    @FXML public void execute() {
        this.threshold();
        this.state.execute();
        this.output.setImage(new Image(AlgoState.OUTPUT_FOLDER + this.state.getOutput()));
        this.count.setText(this.state.getCount().toString());
    }

    /**
     * Threshold current input.
     **/
    @FXML public void threshold() {
        //this.state.createScoreMap();
        //this.state.experimentalThresh();
        //this.state.thresholdImage();
        //this.threshold.setImage(new Image(AlgoState.THRESH_FOLDER + this.state.getThreshold()));
        this.state.loopOver();
    }

    /**
     * Simple function that aggregates values from the controls and changes the state variables
     * This should be called before every algorithm execution
     **/
    public void updateState() {
        String algo = this.algoList.getSelectionModel().getSelectedItem();
        String fname = this.inputList.getSelectionModel().getSelectedItem();
        int hue_min = (int) this.hueSlider_min.getValue();
        int hue_max = (int) this.hueSlider_max.getValue();
        int sat_min = (int) this.satSlider_min.getValue();
        int sat_max = (int) this.satSlider_max.getValue();
        int val_min = (int) this.valSlider_min.getValue();
        int val_max = (int) this.valSlider_max.getValue();

        //singleton
        if (this.state == null)
            this.state = new AlgoState();

        this.state.setAlgoByValue(algo);
        this.state.setInput(fname);
        this.state.setHSV(hue_min, hue_max, sat_min, sat_max, val_min, val_max);
        this.input.setImage(new Image(AlgoState.INPUT_FOLDER + fname));

        System.out.println("Algo: " + algo +" execution");
        System.out.println("img: " + fname);
        System.out.println("hue: " + hue_min + " to " + hue_max);
        System.out.println("sat: " + sat_min + " to " + sat_max);
        System.out.println("val: " + val_min + " to " + val_max);
        System.out.println();
    }
}