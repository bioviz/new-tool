import java.util.stream.LongStream;

public class Statistics {

   static public long[] getHistMean(long[][] hist) {
    	long[] means = new long[3];
    	
    	for(int i = 0; i < 3; i++) {
    	   long sum = 0;
    	   long count = 0;
    		for(int j = 0; j < 256; j++) {
    		   sum += hist[i][j] * j;
    		   count += hist[i][j];
    		}
    		means[i] = sum / count;
    	}
    	
    	return means;
	}

   static public long[] getHistStdDeviation(long[][] hist) {
      long[] means = getHistMean(hist);
      long[] stdDev = new long[3];
      
      for (int i = 0; i < 3; i++) {
         long[] temp = new long[256];
         for (int j = 0; j < 256; j++){
            temp[i] = (long) Math.pow((hist[i][j]-means[i]), 2);
         }
         stdDev[i] = (long) Math.sqrt(LongStream.of(temp).sum() / 256);
      }
      
      return stdDev;
   }
}
