import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat4;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

//"C:/Users/Kishan/Documents/repos/new-tool/src/"

public class AlgoState {
	
	 public static final String INPUT_FOLDER = "inputs/";
	 public static final String THRESH_FOLDER = "thresholds/";
    public static final String EXP_THRESH_FOLDER = "expthresh/";
    public static final String DIFFS_FOLDER = "diffs/";
    public static final String RGB_DIFFS = DIFFS_FOLDER + "rgbdiffs/";
    public static final String HSV_DIFFS = DIFFS_FOLDER + "hsvdiffs/";
    public static final String YRCB_DIFFS = DIFFS_FOLDER + "yrcbdiffs/";
    public static final String HLS_DIFFS = DIFFS_FOLDER + "hlsdiffs/";
    public static final String XYZ_DIFFS = DIFFS_FOLDER + "xyzdiffs/";
    public static final String LAB_DIFFS = DIFFS_FOLDER + "labdiffs/";
    public static final String LUV_DIFFS = DIFFS_FOLDER + "luvdiffs/";

    public static final String OTSU_FOLDER = "otsu/";
    public static final String SINGLE_FOLDER = "single/";
    public static final String INRANGE_FOLDER = "inrange/";    
    public static final String COLOR_SPACES = "colorspaces/";
    public static final String ABS_DIFFS = "absdiffs/";
	 public static final String OUTPUT_FOLDER = "outputs/";
	 public static final String HIST_FOLDER = "histograms/";
	 public static final String CROP_FOLDER = "inputs_cropped/";

    public static final String PROJECT_ROOT = "C:/Users/Dan/Documents/new-tool/src/";
    public static final String INPUT_PATH_ROOT = PROJECT_ROOT + INPUT_FOLDER;
    public static final String THRESH_PATH_ROOT = PROJECT_ROOT + THRESH_FOLDER;
    public static final String EXP_THRESH_ROOT = PROJECT_ROOT + EXP_THRESH_FOLDER;
    public static final String OUTPUT_PATH_ROOT = PROJECT_ROOT + OUTPUT_FOLDER;
    public static final String HIST_PATH_ROOT = PROJECT_ROOT + HIST_FOLDER;
    public static final String CROP_PATH_ROOT = PROJECT_ROOT + CROP_FOLDER;

    //State variables (inputs)
    private int algo;
    private String inputName;
    private int hue_min, sat_min, val_min;
    private int hue_max, sat_max, val_max;
    private FeatureDetector fd = FeatureDetector.create(FeatureDetector.MSER);
    private ArrayList<ColorspaceData> clrList = new ArrayList<ColorspaceData>();
    private int clrSpaceCount = 0;
    //TODO: add morphological ops

    //Intermediary variables
    private Mat inputMat;
    private Mat threshMat;
    private Mat outputMat;
    private MatOfKeyPoint points = new MatOfKeyPoint();

    //Output variables
    private String thresholdName;
    private String outputName;
    private Integer count;
    
    //Algorithms Hash Table
    private HashMap<String, Integer> algorithms;
    private HashMap<String, Integer> inputs;
    
    //Method to Inititialize HashTable
    public void InitializeHashTables() {
    	this.algorithms = new HashMap<String, Integer>();
        this.inputs = new HashMap<String, Integer>();
    	
        //48 Algos
        this.algorithms.put("AKAZE", FeatureDetector.AKAZE);
        this.algorithms.put("BRISK", FeatureDetector.BRISK);
        this.algorithms.put("DENSE", FeatureDetector.DENSE);
        this.algorithms.put("DYNAMIC_AKAZE", FeatureDetector.DYNAMIC_AKAZE);
        this.algorithms.put("DYNAMIC_BRISK", FeatureDetector.DYNAMIC_BRISK);
        this.algorithms.put("DYNAMIC_DENSE", FeatureDetector.DYNAMIC_DENSE);
        this.algorithms.put("DYNAMIC_FAST", FeatureDetector.DYNAMIC_FAST);
        this.algorithms.put("DYNAMIC_GFTT", FeatureDetector.DYNAMIC_GFTT);
        this.algorithms.put("DYNAMIC_HARRIS", FeatureDetector.DYNAMIC_HARRIS);
        this.algorithms.put("DYNAMIC_MSER", FeatureDetector.DYNAMIC_MSER);
        this.algorithms.put("DYNAMIC_ORB", FeatureDetector.DYNAMIC_ORB);
        this.algorithms.put("DYNAMIC_SIFT", FeatureDetector.DYNAMIC_SIFT);
        this.algorithms.put("DYNAMIC_SIMPLEBLOB", FeatureDetector.DYNAMIC_SIMPLEBLOB);
        this.algorithms.put("DYNAMIC_STAR", FeatureDetector.DYNAMIC_STAR);
        this.algorithms.put("DYNAMIC_SURF", FeatureDetector.DYNAMIC_SURF);
        this.algorithms.put("FAST", FeatureDetector.FAST);
        this.algorithms.put("GFTT", FeatureDetector.GFTT);
        this.algorithms.put("GRID_AKAZE", FeatureDetector.GRID_AKAZE);
        this.algorithms.put("GRID_BRISK", FeatureDetector.GRID_BRISK);
        this.algorithms.put("GRID_DENSE", FeatureDetector.GRID_DENSE);
        this.algorithms.put("GRID_FAST", FeatureDetector.GRID_FAST);
        this.algorithms.put("GRID_GFTT", FeatureDetector.GRID_GFTT);
        this.algorithms.put("GRID_HARRIS", FeatureDetector.GRID_HARRIS);
        this.algorithms.put("GRID_MSER", FeatureDetector.GRID_MSER);
        this.algorithms.put("GRID_ORB", FeatureDetector.GRID_ORB);
        this.algorithms.put("GRID_SIFT", FeatureDetector.GRID_SIFT);
        this.algorithms.put("GRID_SIMPLEBLOB", FeatureDetector.GRID_SIMPLEBLOB);
        this.algorithms.put("GRID_STAR", FeatureDetector.GRID_STAR);
        this.algorithms.put("GRID_SURF", FeatureDetector.GRID_SURF);
        this.algorithms.put("HARRIS", FeatureDetector.HARRIS);
        this.algorithms.put("MSER", FeatureDetector.MSER);
        this.algorithms.put("ORB", FeatureDetector.ORB);
        this.algorithms.put("PYRAMID_AKAZE", FeatureDetector.PYRAMID_AKAZE);
        this.algorithms.put("PYRAMID_BRISK", FeatureDetector.PYRAMID_BRISK);
        this.algorithms.put("PYRAMID_DENSE", FeatureDetector.PYRAMID_DENSE);
        this.algorithms.put("PYRAMID_FAST", FeatureDetector.PYRAMID_FAST);
        this.algorithms.put("PYRAMID_GFTT", FeatureDetector.PYRAMID_GFTT);
        this.algorithms.put("PYRAMID_HARRIS", FeatureDetector.PYRAMID_HARRIS);
        this.algorithms.put("PYRAMID_MSER", FeatureDetector.PYRAMID_MSER);
        this.algorithms.put("PYRAMID_ORB", FeatureDetector.PYRAMID_ORB);
        this.algorithms.put("PYRAMID_SIFT", FeatureDetector.PYRAMID_SIFT);
        this.algorithms.put("PYRAMID_SIMPLEBLOB", FeatureDetector.PYRAMID_SIMPLEBLOB);
        this.algorithms.put("PYRAMID_STAR", FeatureDetector.PYRAMID_STAR);
        this.algorithms.put("PYRAMID_SURF", FeatureDetector.PYRAMID_SURF);
        this.algorithms.put("SIFT", FeatureDetector.SIFT);
        this.algorithms.put("SIMPLEBLOB", FeatureDetector.SIMPLEBLOB);
        this.algorithms.put("STAR", FeatureDetector.STAR);
        this.algorithms.put("SURF", FeatureDetector.SURF);

        //this.inputs.put("CC_RED_0.jpg", null);
        this.inputs.put("326_CC_RED_1.jpg", 326);
        this.inputs.put("326_CC_RED_2.jpg", 326);
        //this.inputs.put("CC_RED_3.jpg", null);
        this.inputs.put("241_CC_RED.jpg", 241);
        this.inputs.put("10_CC_RED.jpg", 10);
        this.inputs.put("50_CC_RED.jpg", 50);  //Original Test Case
        this.inputs.put("190_CC_WHITE.jpg", 190);
        this.inputs.put("CC_WHITE_1.jpg", null);
        this.inputs.put("45_CC_WHITE.jpg", 45);
        this.inputs.put("50_CC_WHITE.jpg", 50);
        //this.inputs.put("59_CC_WHITE.jpg", 59);
        this.inputs.put("113_CC_WHITE.jpg", 113);
        this.inputs.put("132_CC_WHITE.jpg", 132);      
        this.inputs.put("140_CC_WHITE.jpg", 140);
        this.inputs.put("146_CC_WHITE.jpg", 146);
        this.inputs.put("436_CC_WHITE.jpg", 436);
        this.inputs.put("984_CC_WHITE.jpg", 984);
        this.inputs.put("1010_CC_WHITE.jpg", 1010);

        // for ( String key : this.inputs.keySet() ) {
        // 	System.out.println(key);
        // 	try {
        // 		outlinePetriDish( key );
        // 	} catch(Exception e) {
        // 		System.out.println("failed");
        // 	}
        // }
    }

    public AlgoState() {
    	this.inputMat = new Mat();
    	this.threshMat = new Mat();
    	this.outputMat = new Mat();
    	this.InitializeHashTables();
    }
    
    public Set<String> getAlgoSet() {
    	SortedSet<String> algo_set = new TreeSet<String>();
    	algo_set.addAll(this.algorithms.keySet());
        return algo_set;
    }

    public Set<String> getInputSet() {
        SortedSet<String> input_set = new TreeSet<String>();
        input_set.addAll(this.inputs.keySet());
        return input_set;
    }

    //Function to set algorithm using Hashtable values instead of a switch statement
    public void setAlgoByValue(String algoName) {
        this.algo = algorithms.get(algoName);
    }

    public void setInput(String fname) {
    	//fname = new StringBuilder(fname).insert(fname.indexOf('.'), "_cropped").toString();
    	//System.out.println("SETTING INPUT TO: " + fname);
    	
        this.inputName = this.outputName = this.thresholdName = fname;
        this.inputMat = Imgcodecs.imread(AlgoState.INPUT_PATH_ROOT + fname, Imgcodecs.CV_LOAD_IMAGE_COLOR);
        
        if (this.inputMat.empty()) {
        	System.out.println("Invalid filename");
        }
    }

    public void setHSV(int hmin, int hmax, int smin, int smax, int vmin, int vmax) {
        this.hue_min = hmin;
        this.hue_max = hmax;
        this.sat_min = smin;
        this.sat_max = smax;
        this.val_min = vmin;
        this.val_max = vmax;
    }

    public String getThreshold() {
        return this.thresholdName;
    }

    public String getOutput() {
        return this.outputName;
    }

    public Integer getCount() {
        return this.count;
    }

    public void thresholdImage() {
        //Mat hsvMat = new Mat();
        Mat hsvMat = inputMat;
        Mat testMat = new Mat();

        System.out.println("channels: " + this.inputMat.channels());
        System.out.println("columns: " + this.inputMat.cols());

        //Imgproc.cvtColor(this.inputMat, hsvMat, Imgproc.COLOR_BGR2HSV);
        Core.inRange(hsvMat, new Scalar(hue_min, sat_min, val_min),
        		new Scalar(hue_max, sat_max, val_max), this.threshMat);
        
        double otsu = Imgproc.threshold(this.threshMat, testMat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        Imgproc.threshold(this.threshMat, testMat, otsu, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        
        this.thresholdName = this.inputName;

        if (!Imgcodecs.imwrite(THRESH_PATH_ROOT + this.thresholdName, testMat)) {
        	System.out.println("Failed to write threshold image");
        }
    }
    
    //TODO:
    public void outlinePetriDish(String fname) {
    	 Mat image = new Mat();
    	 Mat grayImage = new Mat();
    	 Mat finalImage = new Mat();
    	 //Mat croppedImage = new Mat();

    	 /// Read the image (NEED TO CHANGE FILENAME)
    	 image = Imgcodecs.imread(INPUT_PATH_ROOT + fname, Imgcodecs.CV_LOAD_IMAGE_COLOR);

    	 //Check if reading the image worked
    	 System.out.println(image.empty());
    	 
    	 //Print out the image size
    	 System.out.println("Image size: " + image.cols() + "x" + image.rows());
    	 
    	 //Initialize the mask
    	 //mask.zeros(image.rows(), image.cols(), Core.LINE_8);
    	 //Mat mask = Mat.zeros(image.rows(), image.cols(), Core.LINE_8);
    	 //Mat croppedImage = Mat.zeros(image.rows(), image.cols(), Core.LINE_8);
    	 
    	 /// Convert to grayscale
    	 Imgproc.cvtColor(image, grayImage, Imgproc.COLOR_BGR2GRAY);

    	 /// Reduce the noise so we avoid false circle detection
    	 Imgproc.GaussianBlur(grayImage, grayImage, new Size(9, 9), 2, 2 );

    	 MatOfFloat4 circles = new MatOfFloat4();

    	 /// Apply the Hough Transform to find the circles
    	 Imgproc.HoughCircles(grayImage, circles, Imgproc.HOUGH_GRADIENT,1, image.rows(),
    	            100, 10, image.rows()/4, image.rows()/2);

    	 finalImage = grayImage;
    	 
    	 //Output number of circles detected
    	 System.out.println("Number of circles detected: " + circles.cols());
    	 
    	 /// Draw the circles detected
    	 for(int i = 0; i < circles.cols(); i++ )
    	 {
    		 Point center = new Point((int) circles.get(0, i)[0], (int) circles.get(0, i)[1]);
    	     int radius = (int) circles.get(0, i)[2];
    	     //Imgproc.circle(finalImage, center, 3, new Scalar(0, 255, 0), 3, 8, 0);
    	     //Imgproc.circle(finalImage, center, radius, new Scalar(0, 0, 255), 3, 8, 0);
    	 }
    	 
    	 // Fill out the mask to crop the image
    	 /*
    	 {
    		 Point center = new Point((int) circles.get(0, 0)[0], (int) circles.get(0, 0)[1]);
    		 int radius = (int) circles.get(0, 0)[2];
    		 Imgproc.circle(mask, center, radius, new Scalar(255, 255, 255), 3, 8, 0);
    		 Imgcodecs.imwrite("src/Mask.jpg", mask);
    	 }
    	 */
    	 
    	 // Set up the coordinates for the bounding box
    	 int x = (int) circles.get(0, 0)[0]; 
    	 int y = (int) circles.get(0, 0)[1];
	     int radius = (int) circles.get(0, 0)[2];
	     Point upperLeft = new Point(x - radius, y - radius);
	     Point lowerRight = new Point(x + radius, y + radius);
	     
    	 // Draw a bounding box rectangle
    	 Imgproc.rectangle(finalImage, upperLeft, lowerRight, new Scalar(0, 0, 255), 3, 8, 0);
    	 
    	 // Instantiate a new box for the ROI
    	 int height = 2*radius;
    	 int width = 2*radius;
    	 Rect ROI = new Rect(x - radius, y - radius, width, height);
    	 
    	  /// Show your results
    	 //Imgcodecs.imwrite("src/Final_Cell_colony.jpg", finalImage);
    	 
    	 // Crop the image using ROI and show results
    	 Mat croppedImage = new Mat(image, ROI);
    	 Imgcodecs.imwrite(CROP_PATH_ROOT + fname, croppedImage);
    	 
    	 // Crop the image and show results
    	 //grayImage.copyTo(croppedImage, mask);
    	 //Imgcodecs.imwrite("src/Final_Cell_colony_cropped.jpg", croppedImage);
    }

    static public long[][] channelToHist(ArrayList<Mat> src, String fname) {
    	long[][] hist = new long[3][256];
    	
    	try {
    		BufferedWriter fw = new BufferedWriter(new FileWriter(new File(fname)));
            
            for (int k = 0; k < hist.length; k++) {
                for (int i = 0; i < src.get(k).rows(); i++) {
                    for (int j = 0; j < src.get(k).cols(); j++) {
                        ++hist[k][(int)src.get(k).get(i, j)[0]];
                    }
                }
            }
        	
            for (int i = 0; i < 256; i++) {
                fw.write(String.format("%d, %d, %d \n", hist[0][i], hist[1][i], hist[2][i]));
            }

            fw.close();
    	}
    	catch(IOException e) {
    		e.printStackTrace();
    	}
    	
    	return hist;    	
    }

    public void testThresh(String input) {
        Mat image = Imgcodecs.imread(INPUT_PATH_ROOT + input, Imgcodecs.CV_LOAD_IMAGE_COLOR);
        Mat otsuThresh = new Mat();
        Mat singleThresh = new Mat();
        Mat inrangeThresh = new Mat();
        double otsu;
        
        Mat hsvImage = new Mat();
        Mat hsvSat;
        ArrayList<Mat> hsv = new ArrayList<Mat>();
        hsv.add(new Mat());
        hsv.add(new Mat());
        hsv.add(new Mat());
        Imgproc.cvtColor(image, hsvImage, Imgproc.COLOR_BGR2HSV);
        Core.split(hsvImage, hsv);
        hsvSat = hsv.get(1);

        Mat hlsImage = new Mat();
        Mat hlsSat;
        ArrayList<Mat> hls = new ArrayList<Mat>();
        hls.add(new Mat());
        hls.add(new Mat());
        hls.add(new Mat());
        Imgproc.cvtColor(image, hlsImage, Imgproc.COLOR_BGR2HLS);
        Core.split(hlsImage, hls);
        hlsSat = hls.get(2);

        //Test three threshing methods (hsv)
        otsu = Imgproc.threshold(hsvSat, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        otsu = Imgproc.threshold(hsvSat, otsuThresh, otsu, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        Imgproc.threshold(hsvSat, singleThresh, this.sat_min, 255, Imgproc.THRESH_BINARY);
        Core.inRange(hsvSat, new Scalar(this.sat_min), new Scalar(this.sat_max), inrangeThresh);

        otsuThresh = new Mat();
        singleThresh = new Mat();
        inrangeThresh = new Mat();

        //Test three threshing methods (hls)
        otsu = Imgproc.threshold(hlsSat, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        //otsu = Imgproc.threshold(hlsSat, otsuThresh, otsu, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        Imgproc.threshold(hlsSat, singleThresh, this.sat_min, 255, Imgproc.THRESH_BINARY);
        Core.inRange(hlsSat, new Scalar(this.sat_min), new Scalar(this.sat_max), inrangeThresh);

        //write out
        Imgcodecs.imwrite(EXP_THRESH_ROOT + OTSU_FOLDER +"hsv_" + input +"(" +otsu +").jpg", otsuThresh);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + SINGLE_FOLDER +"hsv_" + input +"_single.jpg", singleThresh);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + INRANGE_FOLDER +"hsv_" + input +"_ir.jpg", inrangeThresh);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + OTSU_FOLDER +"hls_" + input +"(" +otsu +").jpg", otsuThresh);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + SINGLE_FOLDER +"hls_" + input +"_single.jpg", singleThresh);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + INRANGE_FOLDER +"hls_" + input +"_ir.jpg", inrangeThresh);
    }

    public void testDiffThresh(String input) {
        System.out.println("Image Name: " + input);
        Mat image = Imgcodecs.imread(INPUT_PATH_ROOT + input, Imgcodecs.CV_LOAD_IMAGE_COLOR);
        clrList.add(new ColorspaceData(input));
        double[] vals = new double[3];
        
        Mat absdifft = new Mat();
        Mat otsuThresh = new Mat();
        Mat hsvImage = new Mat();
        Mat yCrCbImage = new Mat();
        Mat hlsImage = new Mat();
        Mat ciexyzImage = new Mat();
        Mat cielabImage = new Mat();
        Mat cieluvImage = new Mat();

        ArrayList<Mat> rgb = new ArrayList<Mat>();
        ArrayList<Mat> hsv = new ArrayList<Mat>();
        ArrayList<Mat> ycrcb = new ArrayList<Mat>();
        ArrayList<Mat> hls = new ArrayList<Mat>();
        ArrayList<Mat> xyz = new ArrayList<Mat>();
        ArrayList<Mat> lab = new ArrayList<Mat>();
        ArrayList<Mat> luv = new ArrayList<Mat>();

        rgb.add(new Mat());
        rgb.add(new Mat());
        rgb.add(new Mat());

        hsv.add(new Mat());
        hsv.add(new Mat());
        hsv.add(new Mat());

        ycrcb.add(new Mat());
        ycrcb.add(new Mat());
        ycrcb.add(new Mat());

        hls.add(new Mat());
        hls.add(new Mat());
        hls.add(new Mat());

        xyz.add(new Mat());
        xyz.add(new Mat());
        xyz.add(new Mat());

        lab.add(new Mat());
        lab.add(new Mat());
        lab.add(new Mat());

        luv.add(new Mat());
        luv.add(new Mat());
        luv.add(new Mat());

        Imgproc.cvtColor(image, hsvImage, Imgproc.COLOR_BGR2HSV);
        Imgproc.cvtColor(image, yCrCbImage, Imgproc.COLOR_BGR2YCrCb);
        Imgproc.cvtColor(image, hlsImage, Imgproc.COLOR_BGR2HLS);
        Imgproc.cvtColor(image, ciexyzImage, Imgproc.COLOR_BGR2XYZ);
        Imgproc.cvtColor(image, cielabImage, Imgproc.COLOR_BGR2Lab);
        Imgproc.cvtColor(image, cieluvImage, Imgproc.COLOR_BGR2Luv);

        Core.split(image, rgb);
        Core.split(hsvImage, hsv);
        Core.split(yCrCbImage, ycrcb);
        Core.split(hlsImage, hls);
        Core.split(ciexyzImage, xyz);
        Core.split(cielabImage, lab);
        Core.split(cieluvImage, luv);
        double otsu;

        System.out.println("rgb");
        for (int i=0; i < rgb.size(); ++i) {
            for (int j=i+1; j < rgb.size(); ++j) {
                Core.absdiff(rgb.get(i), rgb.get(j), absdifft);
                otsu = Imgproc.threshold(absdifft, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgcodecs.imwrite(EXP_THRESH_ROOT +RGB_DIFFS +"/(" +i +"," +j  +")/" +input +otsu +".jpg", otsuThresh);
                vals[i+j-1] = runAlgo(otsuThresh, this.inputs.get(input));
                //System.out.println("(" +i +"," +j  +"): " +runAlgo(otsuThresh, this.inputs.get(input)));

                absdifft = new Mat();
                otsuThresh = new Mat();
            }
        }
        if(clrList.get(clrSpaceCount) == null) {
           System.out.println("Dan fucked up");
        }
        clrList.get(clrSpaceCount).addScalar("rgb", vals);

        System.out.println("hsv");
        for (int i=0; i < hsv.size(); ++i) {
            for (int j=i+1; j < hsv.size(); ++j) {
                Core.absdiff(hsv.get(i), hsv.get(j), absdifft);
                otsu = Imgproc.threshold(absdifft, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgcodecs.imwrite(EXP_THRESH_ROOT +HSV_DIFFS +"/(" +i +"," +j  +")/" +input +otsu +".jpg", otsuThresh);
                vals[i+j-1] = runAlgo(otsuThresh, this.inputs.get(input));
                //System.out.println("(" +i +"," +j  +"): " +runAlgo(otsuThresh, this.inputs.get(input)));

                absdifft = new Mat();
                otsuThresh = new Mat();
            }
        }
        clrList.get(clrSpaceCount).addScalar("hsv", vals);

        System.out.println("ycrcb");
        for (int i=0; i < ycrcb.size(); ++i) {    
            for (int j=i+1; j < ycrcb.size(); ++j) {
                Core.absdiff(ycrcb.get(i), ycrcb.get(j), absdifft);
                otsu = Imgproc.threshold(absdifft, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgcodecs.imwrite(EXP_THRESH_ROOT +YRCB_DIFFS +"/(" +i +"," +j  +")/" +input +otsu +".jpg", otsuThresh);
                vals[i+j-1] = runAlgo(otsuThresh, this.inputs.get(input));
                //System.out.println("(" +i +"," +j  +"): " +runAlgo(otsuThresh, this.inputs.get(input)));

                absdifft = new Mat();
                otsuThresh = new Mat();
            }
        }
        clrList.get(clrSpaceCount).addScalar("ycrcb", vals);

        System.out.println("hls");
        for (int i=0; i < hls.size(); ++i) {
            for (int j=i+1; j < hls.size(); ++j) {
                Core.absdiff(hls.get(i), hls.get(j), absdifft);
                otsu = Imgproc.threshold(absdifft, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgcodecs.imwrite(EXP_THRESH_ROOT +HLS_DIFFS +"/(" +i +"," +j  +")/" +input +otsu +".jpg", otsuThresh);
                vals[i+j-1] = runAlgo(otsuThresh, this.inputs.get(input));
                //System.out.println("(" +i +"," +j  +"): " +runAlgo(otsuThresh, this.inputs.get(input)));

                absdifft = new Mat();
                otsuThresh = new Mat();
            }
        }

        System.out.println("xyz");
        for (int i=0; i < xyz.size(); ++i) {
            for (int j=i+1; j < xyz.size(); ++j) {
                Core.absdiff(xyz.get(i), xyz.get(j), absdifft);
                otsu = Imgproc.threshold(absdifft, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgcodecs.imwrite(EXP_THRESH_ROOT +XYZ_DIFFS +"/(" +i +"," +j  +")/" +input +otsu +".jpg", otsuThresh);
                vals[i+j-1] = runAlgo(otsuThresh, this.inputs.get(input));
                //System.out.println("(" +i +"," +j  +"): " +runAlgo(otsuThresh, this.inputs.get(input)));

                absdifft = new Mat();
                otsuThresh = new Mat();
            }
        }
        clrList.get(clrSpaceCount).addScalar("xyz", vals);

        System.out.println("lab");
        for (int i=0; i < lab.size(); ++i) {
            
            for (int j=i+1; j < lab.size(); ++j) {
                Core.absdiff(lab.get(i), lab.get(j), absdifft);
                otsu = Imgproc.threshold(absdifft, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgcodecs.imwrite(EXP_THRESH_ROOT +LAB_DIFFS +"/(" +i +"," +j  +")/" +input +otsu +".jpg", otsuThresh);
                vals[i+j-1] = runAlgo(otsuThresh, this.inputs.get(input));
                //System.out.println("(" +i +"," +j  +"): " +runAlgo(otsuThresh, this.inputs.get(input)));

                absdifft = new Mat();
                otsuThresh = new Mat();
            }
        }
        clrList.get(clrSpaceCount).addScalar("lab", vals);
        
        System.out.println("luv");
        for (int i=0; i < luv.size(); ++i) {
            for (int j=i+1; j < luv.size(); ++j) {
                Core.absdiff(luv.get(i), luv.get(j), absdifft);
                otsu = Imgproc.threshold(absdifft, otsuThresh, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgcodecs.imwrite(EXP_THRESH_ROOT +LUV_DIFFS +"/(" +i +"," +j  +")/" +input +otsu +".jpg", otsuThresh);
                vals[i+j-1] = runAlgo(otsuThresh, this.inputs.get(input));
                //System.out.println("(" +i +"," +j  +"): " +runAlgo(otsuThresh, this.inputs.get(input)));

                absdifft = new Mat();
                otsuThresh = new Mat();
            }
        }
        clrList.get(clrSpaceCount).addScalar("luv", vals);
        
        ++clrSpaceCount;
        System.gc();
    }

    //loop over images and run whatever is in body
    public void loopOver() {
        for (String key : this.inputs.keySet()) {
            System.out.println(key);
            try {
                testDiffThresh(key);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("failed");
            }
        }
        System.out.println("done");
        for(ColorspaceData c : clrList) {
           c.printContents();
        }
    }

    
    public void experimentalThresh() {
        // Mat diffOtsuH = new Mat();
        Mat diffOtsuS = new Mat();
        // Mat diffOtsuV = new Mat();
       
        Mat hsvImage = new Mat();
        Mat yCrCbImage = new Mat();
        Mat hlsImage = new Mat();
        Mat ciexyzImage = new Mat();
        Mat cielabImage = new Mat();
        Mat cieluvImage = new Mat();

        Mat absDiffHV = new Mat();
        Mat absdifft = new Mat();
        Mat absDiff = new Mat();
        Mat absDiffOtsu = new Mat();
        Mat adaptiveMean = new Mat();
        Mat adaptiveGauss = new Mat();

        ArrayList<Mat> rgb = new ArrayList<Mat>();
        ArrayList<Mat> hsv = new ArrayList<Mat>();
        ArrayList<Mat> ycrcb = new ArrayList<Mat>();
        ArrayList<Mat> hls = new ArrayList<Mat>();
        ArrayList<Mat> xyz = new ArrayList<Mat>();
        ArrayList<Mat> lab = new ArrayList<Mat>();
        ArrayList<Mat> luv = new ArrayList<Mat>();

        rgb.add(new Mat());
        rgb.add(new Mat());
        rgb.add(new Mat());

        hsv.add(new Mat());
        hsv.add(new Mat());
        hsv.add(new Mat());

        ycrcb.add(new Mat());
        ycrcb.add(new Mat());
        ycrcb.add(new Mat());

        hls.add(new Mat());
        hls.add(new Mat());
        hls.add(new Mat());

        xyz.add(new Mat());
        xyz.add(new Mat());
        xyz.add(new Mat());

        lab.add(new Mat());
        lab.add(new Mat());
        lab.add(new Mat());

        luv.add(new Mat());
        luv.add(new Mat());
        luv.add(new Mat());

        Imgproc.cvtColor(this.inputMat, hsvImage, Imgproc.COLOR_BGR2HSV);
        Imgproc.cvtColor(this.inputMat, yCrCbImage, Imgproc.COLOR_BGR2YCrCb);
        Imgproc.cvtColor(this.inputMat, hlsImage, Imgproc.COLOR_BGR2HLS);
        Imgproc.cvtColor(this.inputMat, ciexyzImage, Imgproc.COLOR_BGR2XYZ);
        Imgproc.cvtColor(this.inputMat, cielabImage, Imgproc.COLOR_BGR2Lab);
        Imgproc.cvtColor(this.inputMat, cieluvImage, Imgproc.COLOR_BGR2Luv);

        Core.split(this.inputMat, rgb);
        Core.split(hsvImage, hsv);
        Core.split(yCrCbImage, ycrcb);
        Core.split(hlsImage, hls);
        Core.split(ciexyzImage, xyz);
        Core.split(cielabImage, lab);
        Core.split(cieluvImage, luv);
        double otsu;

        channelToHist(rgb, HIST_PATH_ROOT + "rgb.txt");
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "rgb_R.jpg", rgb.get(0));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "rgb_G.jpg", rgb.get(1));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "rgb_B.jpg", rgb.get(2));
        for (int i=0; i < rgb.size(); ++i) {
            for (int j=i+1; j < rgb.size(); ++j) {
                Core.absdiff(rgb.get(i), rgb.get(j), absdifft);
                Imgcodecs.imwrite(EXP_THRESH_ROOT + ABS_DIFFS + "rgb_" +"(" +i +"," +j  +").jpg", absdifft);
                absdifft = new Mat();
            }
        }        
        
        Core.inRange(rgb.get(2), new Scalar(233), new Scalar(245), absdifft);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "rgb_BDiff.jpg", absdifft);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "rgb_BDiff2.jpg", absdifft);
        
        channelToHist(hsv, HIST_PATH_ROOT + "hsv.txt");
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "hsv_H.jpg", hsv.get(0));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "hsv_S.jpg", hsv.get(1));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "hsv_V.jpg", hsv.get(2));
        for (int i=0; i < hsv.size(); ++i) {
            for (int j=i+1; j < hsv.size(); ++j) {
                Core.absdiff(hsv.get(i), hsv.get(j), absdifft);
                Imgcodecs.imwrite(EXP_THRESH_ROOT + ABS_DIFFS + "hsv_" +"(" +i +"," +j  +").jpg", absdifft);
                absdifft = new Mat();
            }
        }

        channelToHist(ycrcb, HIST_PATH_ROOT + "ycrcb.txt");
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "ycrcb_Y.jpg", ycrcb.get(0));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "ycrcb_Cr.jpg", ycrcb.get(1));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "ycrcb_Cb.jpg", ycrcb.get(2));
        for (int i=0; i < ycrcb.size(); ++i) {
            for (int j=i+1; j < ycrcb.size(); ++j) {
                Core.absdiff(ycrcb.get(i), ycrcb.get(j), absdifft);
                Imgcodecs.imwrite(EXP_THRESH_ROOT + ABS_DIFFS + "ycrcb_" +"(" +i +"," +j  +").jpg", absdifft);
                absdifft = new Mat();
            }
        }

        channelToHist(hls, HIST_PATH_ROOT + "hls.txt");
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "hls_H.jpg", hls.get(0));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "hls_L.jpg", hls.get(1));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "hls_S.jpg", hls.get(2));
        for (int i=0; i < hls.size(); ++i) {
            for (int j=i+1; j < hls.size(); ++j) {
                Core.absdiff(hls.get(i), hls.get(j), absdifft);
                Imgcodecs.imwrite(EXP_THRESH_ROOT + ABS_DIFFS + "hls_" +"(" +i +"," +j  +").jpg", absdifft);
                absdifft = new Mat();
            }
        }
        //THRESH
        otsu = Imgproc.threshold(hls.get(2), diffOtsuS, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        //diffOtsuS = new Mat();
        //Imgproc.threshold(hls.get(2), diffOtsuS, otsu, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + "diffOtsuS.jpg", diffOtsuS);

        Imgproc.threshold(hls.get(2), diffOtsuS, 254, 255, Imgproc.THRESH_BINARY);

        Imgcodecs.imwrite(EXP_THRESH_ROOT + "diffthreshS.jpg", diffOtsuS);

        this.threshMat = diffOtsuS;

        channelToHist(xyz, HIST_PATH_ROOT + "xyz.txt");
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "xyz_X.jpg", xyz.get(0));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "xyz_Y.jpg", xyz.get(1));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "xyz_Z.jpg", xyz.get(2));
        for (int i=0; i < xyz.size(); ++i) {
            for (int j=i+1; j < xyz.size(); ++j) {
                Core.absdiff(xyz.get(i), xyz.get(j), absdifft);
                Imgcodecs.imwrite(EXP_THRESH_ROOT + ABS_DIFFS + "xyz_" +"(" +i +"," +j  +").jpg", absdifft);
                absdifft = new Mat();
            }
        }

        channelToHist(lab, HIST_PATH_ROOT + "lab.txt");
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "lab_L.jpg", lab.get(0));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "lab_A.jpg", lab.get(1));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "lab_B.jpg", lab.get(2));
        for (int i=0; i < lab.size(); ++i) {
            for (int j=i+1; j < lab.size(); ++j) {
                Core.absdiff(lab.get(i), lab.get(j), absdifft);
                Imgcodecs.imwrite(EXP_THRESH_ROOT + ABS_DIFFS + "lab_" +"(" +i +"," +j  +").jpg", absdifft);
                absdifft = new Mat();
            }
        }

        channelToHist(luv, HIST_PATH_ROOT + "luv.txt");
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "luv_L.jpg", luv.get(0));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "luv_U.jpg", luv.get(1));
        Imgcodecs.imwrite(EXP_THRESH_ROOT + COLOR_SPACES + "luv_V.jpg", luv.get(2));
        for (int i=0; i < luv.size(); ++i) {
            for (int j=i+1; j < luv.size(); ++j) {
                Core.absdiff(luv.get(i), luv.get(j), absdifft);
                Imgcodecs.imwrite(EXP_THRESH_ROOT + ABS_DIFFS + "luv_" +"(" +i +"," +j  +").jpg", absdifft);
                absdifft = new Mat();
            }
        }

        Imgcodecs.imwrite(EXP_THRESH_ROOT + "exp_absDiff.jpg", absDiff);
        Imgcodecs.imwrite(EXP_THRESH_ROOT + "exp_absDiff_Otsu.jpg", absDiffOtsu);
        // Imgcodecs.imwrite(EXP_THRESH_ROOT + "exp_adaptiveMean.jpg", adaptiveMean);
        // Imgcodecs.imwrite(EXP_THRESH_ROOT + "exp_adaptiveGauss.jpg", adaptiveGauss);
    }

    public void createScoreMap() {
        Mat temp;
        Mat scoremap = new Mat(this.inputMat.size(), 16);
        Mat out = new Mat(scoremap.size(), CvType.CV_8UC1);

        for (int i = 50; i <= 200; i += 10) {
            temp = new Mat();
            Imgproc.threshold(this.inputMat, temp, i, 1, Imgproc.THRESH_BINARY);
            Core.add(temp, scoremap, scoremap);
        }

        //write scoremap to file
        Imgcodecs.imwrite(EXP_THRESH_ROOT + "scoremap.jpg", scoremap);
        //otsu threshold scoremap
        //scoremap.convertTo(scoremap, CvType.CV_8UC1);
        //double otsu = Imgproc.threshold(scoremap, out, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        //Imgproc.threshold(scoremap, out, otsu, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        //Imgcodecs.imwrite(EXP_THRESH_ROOT + "scoremapout.jpg", out);
    }

    //TODO: Implement algorithm execute function(s)
        //This may vary depending on the algorithm... may need multiple
        //All execute functions must return output image and feature count
    public void execute() {
        this.fd = FeatureDetector.create(this.algo);  
        points = new MatOfKeyPoint();
        
        this.fd.detect(threshMat, points);
        Features2d.drawKeypoints(inputMat, points, outputMat, new Scalar(0, 0, 255), Features2d.DRAW_RICH_KEYPOINTS);
        
        try {
        	Imgcodecs.imwrite(OUTPUT_PATH_ROOT + outputName, outputMat);
        } catch(Exception e) {
        	System.out.println("Failed to write output image");
        	e.printStackTrace();
        }
        this.count = (int) this.points.total();
    }

    public int runAlgo(Mat threshold, int expected) {
        this.fd.detect(threshold, points);
        return expected - ((int) this.points.total());
    }
}
